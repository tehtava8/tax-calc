#!/bin/bash

# GIT project
git init
echo "node_modules" >> .gitignore
echo ".DS_Store" >> .gitignore
echo "public" >> .gitignore

# NPM initializing and libraries
npm init -y
npm i --save-dev express mocha chai

# Project structures
mkdir src test
touch dev-server.mjs
touch src/index.mjs src/index.html src/index.css src/tax.mjs
touch test/tax.spec.mjs

# NPM project details
npm pkg set description="Tax calculator"
npm pkg set author="Heidi Laaksonen_HERE"
npm pkg set license="MIT"

# ECMAScript approach
npm pkg set type="module"

# NPM scripts
npm pkg set scripts.start="node dev-server.mjs"
npm pkg set scripts.test="mocha"
npm pkg set scripts.build="mkdir -p public && rm -f public/* && cp -r src/* public"
npm pkg set scripts.dev="node --watch-path=src --watch dev-server.mjs"

# Alempi ajaa tiedoston suoraan
#./commands.sh
# JOS tulee vastaan permission denied alempi saattaa toimia (mulla ei tullu)
#chmod 740 commands.sh

# git remote add origin https://gitlab.com/tehtava8/tax-calc
# git remote -v 

# jos pitää muuttaa 
# git remote set-url origin https://gitlab.com/tehtava8/tax-calc

# git status
# git add .
# git commit -m "initial commit"
# git push
# git push --set-upstream origin master

# git tag -a v1.0 -m 'version 1.0'
# git tag --list
# git push origin v1.0 